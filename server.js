const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors') ;
const MongoClient = require('mongodb').MongoClient
const port = process.env.PORT || 5000
const dbName = 'poll_data'
const collectionName = 'messages'
var app = express()

const connectionString = process.env.mongoURL || 'mongodb+srv://cluster:omPbYHqOLy8mlLbf@poll-api-message.cvbk4.mongodb.net/poll_data?retryWrites=true&w=majority';

MongoClient.connect(connectionString, { useUnifiedTopology: true })
  .then(client => {
    console.log('Connected to Database')
    const db = client.db(dbName) ;

    app.set('view engine', 'ejs')
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())
    app.use(cors())

    app.get('/', (req, res) => {
      res.render('index') ;
    })

    app.get('/getParticipantActions/', (req, res) => {
 
        let roomId = req.query.roomId.toString();
        let timestamp = parseInt(req.query.timestamp);
        db
        .collection(collectionName)
        .find({"roomId":roomId, "last_modified": {$gte: timestamp}})
        .toArray(function (err, result) {
            if (err) {
                return res.status(400).send(err);
            } else {
                console.log(typeof timestamp) ;
                return res.json(result);
            }
        }) ;
    }); 

    app.post('/postParticipantActions', (req, res) => {

        const matchDocument = {
            uid : req.body.uid,
            roomId : req.body.roomId,
            message : req.body.message,
            last_modified: new Date().getTime()
        };

        db
        .collection(collectionName)
        .insertOne(matchDocument, function (err, result) {
            if (err) {
                return res.status(400).send(err);
            } else {
                return res.status(204).send("Added");
            }
        });
    });

    app.listen(port, function () {
      console.log(`listening on ${port}`)
    })
  })
  .catch(console.error)